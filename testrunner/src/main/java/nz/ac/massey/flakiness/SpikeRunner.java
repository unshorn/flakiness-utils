package nz.ac.massey.flakiness;

import edu.illinois.cs.testrunner.coreplugin.TestPluginUtil;
import edu.illinois.cs.testrunner.data.framework.TestFramework;
import edu.illinois.cs.testrunner.data.results.Result;
import edu.illinois.cs.testrunner.data.results.TestResult;
import edu.illinois.cs.testrunner.data.results.TestRunResult;
import edu.illinois.cs.testrunner.coreplugin.TestPlugin;
import edu.illinois.cs.testrunner.runner.Runner;
import edu.illinois.cs.testrunner.runner.RunnerFactory;
import edu.illinois.cs.testrunner.testobjects.TestLocator;
import edu.illinois.cs.testrunner.util.ProjectWrapper;
import scala.collection.JavaConverters;
import scala.util.Try;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

public class SpikeRunner extends TestPlugin {
    private static Map<Integer, List<String>> locateTestList = new HashMap<>();
    Runner runner;
    @Override
    public void execute(ProjectWrapper project)  {
        List<Runner> runners = RunnerFactory.allFrom(project);
        try {
            runners = removeZombieRunners(runners, project);
        } catch (Exception e) {

        }
        Map<String,Integer> counts = new HashMap<>();

        try {
            runner = runners.get(0);
            System.err.println(runner);
            System.err.println(runner.framework());

            final List<String> tests = getOriginalOrder(project, this.runner.framework());
            List<String> testClasses = new ArrayList<>();
            HashMap<String, List<String>> testMethods = new HashMap<>();
            for(String testName: tests) {
                int i = testName.lastIndexOf(".");
                assert i != -1;
                String testClass = testName.substring(0, i);
                String testMethod = testName.substring(i+1, testName.length());
                if (!testClasses.contains(testClass))
                    testClasses.add(testClass);
                List<String> methods = testMethods.get(testClass);
                if (methods == null) {
                    methods = new ArrayList<>();
                    testMethods.put(testClass, methods);
                }
                if (!methods.contains(testMethod)) {
                    methods.add(testMethod);
                }

            }
            Collections.shuffle(testClasses);
            List<String> shuffledTests = new ArrayList<>();
            for(String className: testClasses) {
                Collections.shuffle(testMethods.get(className));
                for(String m: testMethods.get(className)) {
                    String t = className + "." + m;
                    shuffledTests.add(t);
                }
            }

            if (System.getProperty("randomizeTests") != null && System.getProperty("randomizeTests").equals("false")) {
                shuffledTests = tests;
            }

            Try<TestRunResult> testRunResultTry = runner.runList(shuffledTests);
            if (testRunResultTry.isSuccess()) {
                TestRunResult tr = testRunResultTry.get();
                Map<String, TestResult> results = tr.results();
                for (String k : shuffledTests) {
                    if (results.containsKey(k)) {
                        System.err.println(k + " : " + results.get(k).result());
                        Result result = results.get(k).result();
                        if (counts.get(results.get(k).result().toString()) == null) {
                            counts.put(results.get(k).result().toString(), 0);
                        }
                        int current = counts.get(results.get(k).result().toString());
                        counts.put(results.get(k).result().toString(), current+1);
                    }
                }
            }
        } catch (Exception e) {

        }
        System.err.println(counts);

    }

    public static List<String> getOriginalOrder(
            final ProjectWrapper project,
            TestFramework testFramework,
            boolean ignoreExisting) throws IOException {
        if (!Files.exists(Paths.get("original-order")) || ignoreExisting) {
            TestPluginUtil.project.info("Getting original order by parsing logs. ignoreExisting set to: " + ignoreExisting);

            try {
                final Path surefireReportsPath = Paths.get(project.getBuildDirectory()).resolve("surefire-reports");
                final Path mvnTestLog = util.PathManager.parentPath(Paths.get("mvn-test.log"));

                if (Files.exists(mvnTestLog) && Files.exists(surefireReportsPath)) {
                    final List<TestClassData> testClassData = new util.GetMavenTestOrder(surefireReportsPath, mvnTestLog).testClassDataList();

                    final List<String> tests = new ArrayList<>();

                    String delimiter = testFramework.getDelimiter();

                    for (final TestClassData classData : testClassData) {
                        for (final String testName : classData.testNames) {
                            tests.add(classData.className + delimiter + testName);
                        }
                    }

                    return tests;
                } else {
                    return locateTests(project, testFramework);
                }
            } catch (Exception ignored) {}

            return locateTests(project, testFramework);
        } else {
            return Files.readAllLines(Paths.get("original-order"));
        }
    }


    public static List<String> getOriginalOrder(
            final ProjectWrapper project,
            TestFramework testFramework) throws IOException {
        return getOriginalOrder(project, testFramework, false);
    }


    private static List<String> locateTests(ProjectWrapper project,
                                            TestFramework testFramework) {
        int id = Objects.hash(project, testFramework);
        if (!locateTestList.containsKey(id)) {
            TestPluginUtil.project.info("Locating tests...");
            try {
                List<Object> objects = JavaConverters.bufferAsJavaList(TestLocator.tests(project, testFramework).toBuffer());
                ArrayList<String> a = new ArrayList<>();
                for(Object o: objects) {
                    a.add((String) o);
                }
                locateTestList.put(id, new ArrayList<String>(a));


            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
        return locateTestList.get(id);
    }

    private static List<Runner> removeZombieRunners(
            List<Runner> runners, ProjectWrapper project) throws IOException {
        // Some projects may include test frameworks without corresponding tests.
        // Filter out such zombie test frameworks (runners).
        // For example, a project have both JUnit 4 and 5 dependencies, but there is
        // no JUnit 4 tests. In such case, we need to remove the JUnit 4 runner.
        List<Runner> aliveRunners = new ArrayList<>();
        for (Runner runner : runners) {
            if (locateTests(project, runner.framework()).size() > 0) {
                aliveRunners.add(runner);
            }
        }
        return aliveRunners;
    }

  /*  @Override
    public void execute(final MavenProject project) {
        Runner r = RunnerFactory.from(project).get();
        Try<TestRunResult> run = r.run(TestLocator.tests(project, TestFramework.junitTestFramework()));
        if (!run.isFailure()) {
            TestRunResult tr = run.get();
            Map<String, TestResult> results = tr.results();
            for (String k : results.keySet()) {
                System.err.println("%s: %s".format(k, results.get(k).result()));
            }
        }


    }

   */


}
