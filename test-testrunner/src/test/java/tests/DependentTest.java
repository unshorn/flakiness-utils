package tests;

import nz.ac.wgtn.saflate.junit4.DisableFailedDependentTestsRule;
import nz.ac.wgtn.saflate.junit4.PradetTestDependencyGraph;
import nz.ac.wgtn.saflate.junit4.TestDependencyGraph;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;

import static org.junit.Assert.assertTrue;

public class DependentTest {
    static {
        // TODO -- use a better mechanism to select provider, e.g. by using service locator
        TestDependencyGraph.INSTANCE = new PradetTestDependencyGraph();
    }

    @Rule
    public TestRule rule = new DisableFailedDependentTestsRule();

    @Test
    public void test1() {
        assertTrue(false);
    }

    @Test
    public void test2() {
        assertTrue(true);
    }

}
