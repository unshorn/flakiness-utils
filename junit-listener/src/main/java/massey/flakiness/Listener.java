package massey.flakiness;
 
import org.junit.runner.Description;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;
import org.junit.runner.notification.RunListener;

import java.io.IOException;
import java.nio.file.*;
 
public class Listener extends RunListener
{
    /**
     * Called before any tests have been run.
     * */
    public void testRunStarted(Description description) throws java.lang.Exception
    {
    }
 
    /**
     *  Called when all tests have finished
     * */
    public void testRunFinished(Result result) throws java.lang.Exception
    {
    }
 
    /**
     *  Called when an atomic test is about to be started.
     * */
    public void testStarted(Description description) throws java.lang.Exception
    {
        String className = description.getTestClass().getCanonicalName();
        String contentToAppend = className + "." + description.getMethodName() + "\n";
        log(contentToAppend, "order.txt");

    }

    public static void log(String msg, String file) {

        try {
            Files.write(
                    Paths.get(file),
                    msg.getBytes(),
                    StandardOpenOption.APPEND);
       } catch (IOException e) {
        }
    }
 
    /**
     *  Called when an atomic test has finished, whether the test succeeds or fails.
     * */
    public void testFinished(Description description) throws java.lang.Exception
    {
        System.out.println("Finished execution of test case : "+ description.getMethodName());
    }
 
    /**
     *  Called when an atomic test fails.
     * */
    public void testFailure(Failure failure) throws java.lang.Exception
    {
        String className = failure.getDescription().getTestClass().getCanonicalName();
        String contentToAppend = className + "." + failure.getDescription().getMethodName() + ",FAILED\n";
        log(contentToAppend, "results.txt");
    }
 
    /**
     *  Called when a test will not be run, generally because a test method is annotated with Ignore.
     * */
    public void testIgnored(Description description) throws java.lang.Exception
    {
        String className = description.getTestClass().getCanonicalName();
        String contentToAppend = className + "." + description.getMethodName() + ",SKIPPED\n";
        log(contentToAppend, "results.txt");
    }

    public void testAssumptionFailure(Failure failure) {
        String className = failure.getDescription().getTestClass().getCanonicalName();
        String contentToAppend = className + "." + failure.getDescription().getMethodName() + ",SKIPPED\n";
        log(contentToAppend, "results.txt");
    }

}
